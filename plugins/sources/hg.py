"""
hg source
"""

import os

from buildstream import Source, SourceError, Consistency
from buildstream import utils

class HgSource(Source):
    def configure(self, node):
        self.rev = self.node_get_member(node, str, 'rev', None)

        config_keys = ['url', 'track', 'rev']
        self.node_validate(node, config_keys + Source.COMMON_CONFIG_KEYS)

        self.original_url = self.node_get_member(node, str, 'url')
        self.tracking = self.node_get_member(node, str, 'track', None)

        if self.rev is None and self.tracking is None:
            raise SourceError("{}: Hg sources require a ref and/or track".format(self),
            reason="missing-track-and-ref")

        self.mirror = os.path.join(self.get_mirror_directory(),
                utils.url_directory_name(self.original_url))

    def preflight(self):
        # Check if hg is installed, get the binary at the same time
        self.host_hg = utils.get_host_tool('hg')

    def get_unique_key(self):
        key = [self.original_url, self.rev]

        return key

    def get_consistency(self):
        if self.has_ref():
            return Consistency.CACHED
        elif self.rev is not None:
            return Consistency.RESOLVED
        return Consistency.INCONSISTENT

    def fetch(self):
        with self.timed_activity("Fetching {}".format(self.original_url),
                                 silent_nested=True):
            self.ensure()

    def track(self):
        if not self.tracking:
            return None

        resolved_url = self.translate_url(self.original_url)
        with self.timed_activity("Tracking {} from {}"
                                .format(self.tracking, resolved_url),
                                silent_nested=True):
            self.ensure()

            ret = self.latest_commit(self.tracking)

        return ret

    def stage(self, directory):
        with self.timed_activity("Staging {}".format(self.original_url),
                                                     silent_nested=True):
            self.call([self.host_hg, 'clone', '-r', self.rev,
                      self.mirror, directory],
                      fail="Failed to create hg mirror {} in directory: {}"
                      .format(self.mirror, directory),
                      fail_temporarily=True)

    def ensure(self, alias_override=None):
        if not os.path.exists(self.mirror):
            with self.tempdir() as tmpdir:
                url = self.translate_url(self.original_url,
                                         alias_override=alias_override)
                self.call([self.host_hg, 'clone', url, tmpdir],
                                  fail="Failed to clone hg repository {}"
                                  .format(self.original_url),
                                  fail_temporarily=True)
                os.rename(tmpdir, self.mirror)

    def has_ref(self):
        if os.path.exists(self.mirror):
            rc = self.call([self.host_hg, 'parent', '-r', self.rev],
                            cwd=self.mirror)
            return rc == 0
        return False

    def load_ref(self, node):
        self.rev = self.node_get_member(node, str, 'rev', None)

    def get_ref(self):
        return self.rev

    def set_ref(self, ref, node):
        node['rev'] = self.rev = ref

    def latest_commit(self, track):
        _, rev = self.check_output([self.host_hg, 'identify', '--id', '--rev', track],
                           fail="Failed to find latest rev on branch {}".format("track"),
                           cwd=self.mirror)
        rev = rev.strip()
        return rev

def setup():
    return HgSource
