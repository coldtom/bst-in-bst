# BuildStream in BuildStream

A BuildStream project for BuildStream.

## About
[BuildStream](https://gitlab.com/buildstream/buildstream) is a software
integration tool for integrating build pipelines to produce a software stack. In
more basic terms, it's a tool to build a whole bunch of projects at once. This
is a project to integrate a BuildStream stack using BuildStream, the motivation
for which is entirely just that meta things are fun.

## Using
In order to build the project you will need:
* [BuildStream](https://buildstream.build/install.html)
* [bst-external](https://gitlab.com/buildstream/bst-external)
* tar
* git

If you want to produce output that is usable beyond a chroot, this project
supports building a docker image. For this you also require docker ([for now](https://gitlab.com/BuildStream/bst-plugins-container/issues/3))

To simply build the project run:
```make build```
or using BuildStream more explicitly:
```bst build buildstream.bst```

This will give you an artifact that contains (almost) all the dependencies
BuildStream needs to build from any of the sources it has, as well as anything
in bst-external. If you want a less useful (limited to `remote` sources only, I
think) then you can build `buildstream-minimal.bst`.

To produce a docker image run:
```make docker```

To run the docker image run:
```docker run -ti --rm bst-in-bst```

In its current state, the produced BuildStream is capable of building
BuildStream via this project, in case one level of meta was insufficient.
