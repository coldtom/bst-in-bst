SHELL=/bin/bash
ARCH=$(shell uname -m | sed "s/^i.86$$/i686/")
ARCH_OPTS=-o target_arch $(ARCH)
BST=bst --colors $(ARCH_OPTS)
CHECKOUT=buildstream

all: build

build:
	$(BST) build buildstream.bst

checkout: build
	$(BST) checkout --hardlinks buildstream.bst $(CHECKOUT)

$(CHECKOUT): checkout

docker: $(CHECKOUT)
	docker build --tag bst-in-bst .

clean:
	rm -rf $(CHECKOUT)
